#!/bin/bash

#set -x

INPUT1=$(cat $1)
INPUT2=$(cat $2)

#echo first value $((INPUT1))
#echo second value $((INPUT2))

if [ $(($INPUT1)) -eq $(($INPUT2)) ]
then echo "Values ​​are equal" # Немного доработал калькулятор, чтобы он учитывал что результаты вычислений могут быть равны.
elif [ $(($INPUT1)) -gt $(($INPUT2)) ]
then echo $(($INPUT1))
else echo $(($INPUT2))
fi
